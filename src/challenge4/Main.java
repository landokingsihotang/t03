package challenge4;

public class Main {
   public static void main(String[] args) {
      //type casting dilakukan pada method "suara()"
      //ketika yang digunakan hewan.suara();
      //hal ini akan memanggil method pada parent class
      //untuk itu menambahkan (Anjing) untuk melakukan casting
      //agar method yang dipanggil adalah method pada child class
      Hewan hewan = new Anjing();
      ((Anjing)hewan).suara();
   }
}

//Type casting variabel hewan menjadi variabel Anjing