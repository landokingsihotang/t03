package challenge3;

public class Main {
   public static void main(String[] args) {
      //melakukan type casting pada parent class menjadi child class
      //memang tidak ada error pada data dibawah ,namun apabila dijalankan
      //akan mengalami
      //Exception in thread "main" java.lang.ClassCastException: class challenge3.Hewan cannot be cast to class challenge3.Anjing (challenge3.Hewan and challenge3.Anjing 
      // are in unnamed module of loader 'app')
      // at challenge3.Main.main(Main.java:6)
      //hal ini terjadi karena parent class tidak memiliki atribut dari child class
      //sehingga tidak bisa di cast parent class menjadi child class
      //namun sebaliknya bisa

      Anjing anjing = (Anjing) new Hewan();
      anjing.eat();

      //hal yang mungkin bisa dilakukan adalah melakukan casting pada child class menjdi parent class
      //karena child class pasti memiliki atribut dan method dari parent class
      // Hewan hewan = new Anjing();
      // Anjing anjing = (Anjing) hewan;
      // anjing.eat();
   }


}